def is_valid(num: int) -> bool:
        return all(int(a) < int(b) for a, b in zip(str(num), str(num)[1:]))


class odometer:
    def __init__(self, length: int, reading1: int, reading2: int):
        self.length = length
        self.reading1 = reading1
        self.reading2 = reading2
        self.start = int("123456789"[:length])
        self.stop = int("123456789"[-length:])

    def __str__(self):
        return str(self.reading)

    def __repr__(self):
        return f"{self.start} <<<< {self.reading} >>>> {self.stop}"

    def forward(self, steps: int = 1):
        for _ in range(steps):
            if self.reading1 == self.stop:
                return self.start
            else:
                self.reading1 += 1
                while not is_valid(self.reading1):
                    self.reading1 += 1
                return self.reading1

    def backward(self, steps: int = 1):
        for _ in range(steps):
            if self.reading1 == self.start:
                return self.stop
            else:
                self.reading1 -= 1
                while not is_valid(self.reading1):
                    self.reading1 -= 1
                return self.reading1

    def distance(self, reading1: int, reading2: int):
        if len(str(self.reading1)) != len(str(self.reading2)):
            return "Invalid"
        step = self.reading1
        count = 0
        while step != self.reading2:
            count += 1
            step = forward(step)
        return count
